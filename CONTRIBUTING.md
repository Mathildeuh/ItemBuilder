# Contribution

First of all, thank you that you are interested in helping with ItemBuilder. Here are some guidelines for contribution.

### Issues

If you encouter a problem with the ItemBuilder, please create an [issue](https://gitlab.com/KevSlashNull/ItemBuilder/issues). When you feel comfortable to attempt to fix the problem yourself, you can create a [merge request](https://gitlab.com/KevSlashNull/ItemBuilder/merge_requests).

Always check if a similar issue already exists to avoid duplicates.

### Commits

-   Use present tense & imperative mood.
    [✔] "Add Feature"  
    [✖] "Added Feature"  
    [✖] "Adds Feature"
-   When fixing bugs and implementing features, include the issue number.
    [✔] "Fix #245 material is always null"
    [✔] "Fix material is always null" with description "This fixes #245. ..."
    [✖] "Fixed bug with Constructor Material found in #245"
-   If possible, please avoid long commit messages as they appear in `git shortlog`. Keep them short and precise.

Thank you very much for reading (and contributing)! Have a nice day!
